//
//  ColorPickerVC.swift
//  delegate02
//
//  Created by Eddy Rogier on 28/08/2017.
//  Copyright © 2017 EddyRogier. All rights reserved.
//

import UIKit

class ColorPickerVC: UIViewController {

    var delegate:ColorTransfereDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func actionChoiceColor(_ sender: UIButton) {
        
        var nameColor:String = (sender.titleLabel?.text!)!
        print(nameColor)
        self.navigationController?.popViewController(animated: true)
        if delegate != nil {
            self.delegate?.userDidChoose(color:sender.backgroundColor!, nameColor: nameColor)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



