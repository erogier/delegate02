//
//  ViewController.swift
//  delegate02
//
//  Created by Eddy Rogier on 28/08/2017.
//  Copyright © 2017 EddyRogier. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ColorTransfereDelegate {
    
    @IBOutlet weak var colorNameLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func userDidChoose(color: UIColor, nameColor: String) {
        self.view.backgroundColor = color
        self.colorNameLbl.text = nameColor
    }
	
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueColorPickerVC" {
            guard let segueColorPickerVC = segue.destination as? ColorPickerVC else { return }
            segueColorPickerVC.delegate = self
        }
    }
}

