//
//  ColorTransfereDelegate.swift
//  delegate02
//
//  Created by Eddy Rogier on 28/08/2017.
//  Copyright © 2017 EddyRogier. All rights reserved.
//

import Foundation
import UIKit

protocol ColorTransfereDelegate {
    func userDidChoose(color:UIColor, nameColor: String)
}
